/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.test.myservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author PsLgComp
 * 
 * extra info:
 * https://www.tutorialspoint.com/servlets/servlets-first-example.htm
 */
public class FirstServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FirstServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FirstServlet at " + request.getContextPath() + "</h1>");
            out.println("<p>"+ this.getInitParameter("FirstInitParam")+"</p>");
            out.println("<h3>wersja aplikacji: " + getServletContext().getInitParameter("version") + "</h3>");
    
            //wyświetlanie wszystkich parametrów wejściowych z request
            Enumeration<String> params = request.getParameterNames();
            String param;
            while (params.hasMoreElements()){
                param = params.nextElement();
                out.println("<p>parametr " + param + " = "
                        + request.getParameter(param)+ "</p>");     
            }
//            out.println("<h1>Servlet FirstServlet at " + request.getContextPath() + "</h1>");
//            if(request.getParameter("p1")!=null){
//                out.println("<p>parametr p1 = " + request.getParameter("p1") + "</p>");
//            }
//            if(request.getParameter("p2") != null){
//                out.println("<p>parametr p2 = " + request.getParameter("p2") + "</p>");
//            }
            //wyświetlanie linku
            out.println("<h4><a href='"+request.getContextPath()+"/second?source=first&data=(empty)'>Do drugiego servleta</a></h4>");
            out.println("<br/>");
            out.println("<p>Podaj swoje imie wielce szanowny użytkowniku: </p>");
            out.println("<form action='"+request.getContextPath()+"/db' method='post'><input type='text' name='name'/><input type='submit' value='Ok'/></form>");
            
            out.println("<br/>");
            HttpSession session = request.getSession();
            if (session.isNew()) {
                session.setAttribute("licznik", 1);
            } else {
                Integer counter = Integer.valueOf(session.getAttribute("licznik").toString());
                session.setAttribute("licznik", ++counter);
            }
            out.println("<h5>Licznik: " + session.getAttribute("licznik") + "</h5>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
