/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.test.myservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author PsLgComp
 */
@WebServlet(name = "DbServlet", urlPatterns = {"/DbServlet", "/db"})
public class DbServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DbServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DbServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletContext ctx = getServletContext();
        //initialize DB Connection
        String url = "jdbc:mysql://localhost/library_db?serverTimezone=CET";
        try {
            int alteredRowsCount = 0;
            
            boolean atHome = Boolean.parseBoolean(
                    getServletContext().getInitParameter("testsAtHome"));
            DBConnectionManager connectionManager;
            if (atHome) {
                connectionManager = new DBConnectionManager(url, "root", "testroot");
            } else {
                connectionManager = new DBConnectionManager(url, "root", "");
            }
            ctx.setAttribute("DBConnection", connectionManager.getConnection());
            Connection con = (Connection) getServletContext().getAttribute("DBConnection");
            java.sql.PreparedStatement ps = null;
            try {
                ps = con.prepareStatement("insert into library_user(name) values (?)");
                ps.setString(1, request.getParameter("name"));
                ps.execute();
                alteredRowsCount = ps.getUpdateCount();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet DbServlet</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<p> Zmodyfikowano linii : "+alteredRowsCount+"</p>");
                out.println("<p> Sukcessfulnie dodano użytkownika : " + request.getParameter("name") + "</p>");
                out.println("<h4><a href='"+request.getContextPath()+"/first'>Powrót</a></h4>");
                out.println("</body>");
                out.println("</html>");
            }
            
        }catch(ClassNotFoundException|SQLException ex){
            Logger.getLogger(DbServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
